//Exercice1
//#region 
var prompt = require("prompt-sync")();

const a = prompt("Entrer votre nom");
const b = prompt("Entrer votre prenom");
console.log("Addition de deux variables type chaine (Concatenation)");
console.log("Vous avez saisi: "+a);
console.log("Vous avez saisi: "+b);
console.log(`Bonjour ${a} ${b}`);
//#endregion

//Exercice2
//#region
var prompt = require("prompt-sync")();

var a0 = prompt("Entrer un nombre");
var b0 = prompt("Entrer un second nombre");

var c0 = parseInt(a0) + parseInt(b0);

console.log("Vous avez saisi: "+a0);
console.log("Vous avez saisi: "+b0);
console.log(`La somme de ${a0} + ${b0} = ${c0}`);
//#endregion

//Exercice3
//#region
console.log("Utilisation des fonctions() de l'objet Math");
console.log("Utilisation de la constante π(pi)");
console.log(`La valeur de π est ${Math.PI}`);
console.log("Calcul du périmètre (d x π) et de l'aire(π x r²)");
var d1 = 10;
console.log(`Diamètre =${d1}`+"cm");
console.log(`Périmètre =${d1*Math.PI}`+"cm");
console.log(`Aire =${Math.PI*(Math.pow(d1/2,2))}`+"cm²");
console.log(`Périmètre(arrondi) = ${Math.round(d1*Math.PI)}` + "cm");
console.log(`Aire =${Math.round(Math.PI*(Math.pow(d1/2,2)))}`+"cm²");
//#endregion

//Exercice4 
//#region
console.log("Ce mot est il un palindrome?");
var prompt = require("prompt-sync")();

const a2 = prompt("Entrer votre mot");
console.log("Vous avez saisi : "+a2);

console.log("le mot inversé : "+a2.split("").reverse().join(""));

if(a2 == a2.split("").reverse().join(""))
{
console.log(`Le mot ${a2} est un palindrome`);
}
//#endregion


//Exercice9 
//#region
var prompt = require("prompt-sync")();

const c3 = prompt("Entrer le capital de départ");
const t3 = prompt("Entrer le taux d'interet");
const d3 = prompt("Entrer la durée de l'épargne");


console.log(`Avec un capital initial de ${c3}`+"€"+`,placé à ${t3}% pendant ${d3} année(s)`);
//capital de depart = capital * (1+taux interet/100)^duree epargne
console.log(`Le montant total des interets s'élèvera ${(d3*c3*t3)/100}`);
console.log(`Le capital final à l'issue sera de ${c3+(Math.exp((1+(t3/100)),d3))}`);


//#endregion


/*
var nom;
var prenom;

function Affichage(){
    var prompt();
    

}
*/

